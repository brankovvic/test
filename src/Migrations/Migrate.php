<?php

class Migrate {
    public $createCommand;

    public $dropCommand;
    /**
     * @var ConnectionInterface
     */
    private $connection;

    public function __construct(ConnectionInterface $connection)
    {
        //There is some error when creating FK... for the sake of time i will create without
        //never the less there is a model in mysql workbench were you can check structure
        $this->createCommand = [
            'SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;',
            'SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;',
            "SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';",
            "DROP SCHEMA IF EXISTS `mydb` ;",
            "CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;",
            "USE `mydb` ;",
            "CREATE TABLE IF NOT EXISTS `mydb`.`users` (
                `id` INT NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(45) NOT NULL,
                PRIMARY KEY (`id`))
                ENGINE = InnoDB;",
            "CREATE TABLE IF NOT EXISTS `mydb`.`currencies` (
                `id` INT NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(45) NOT NULL,
                `abbreviation` VARCHAR(4) NOT NULL,
                PRIMARY KEY (`id`))
                ENGINE = InnoDB;",
            "CREATE TABLE IF NOT EXISTS `mydb`.`currency_rates` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `source_currency` INT UNSIGNED NOT NULL,
  `target_currency` INT UNSIGNED NOT NULL,
  `rate` DOUBLE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;",
            "CREATE TABLE IF NOT EXISTS `mydb`.`trade` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `trade_currency` INT UNSIGNED NOT NULL,
  `exchange_rate` INT UNSIGNED NOT NULL,
  `surcharge_percent` FLOAT NOT NULL,
  `surcharge_amount` DOUBLE NOT NULL,
  `amount_paid` FLOAT NOT NULL,
  `date_created` DATETIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;",
            "SET SQL_MODE=@OLD_SQL_MODE;",
            "SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;",
            "SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;"
        ];

        $this->dropCommand = [
            "DROP SCHEMA IF EXISTS `mydb` ;"
        ];
        $this->connection = $connection;
    }

    public function migrate() {
        $this->connection->beginTransaction();

        foreach ($this->createCommand as $command) {
            if (is_null($this->connection->query($command))) {

                $this->connection->rollBack();
                throw new Exception("Failed to migrate database");
            }
        }
        $this->connection->commit();
    }

    public function rollback() {
        $this->connection->beginTransaction();

        foreach ($this->dropCommand as $command) {
            if ( is_null($this->connection->query($command))) {
                $this->connection->rollBack();
                throw new Exception("Failed to drop database");
            }
        }
        $this->connection->commit();
    }
}