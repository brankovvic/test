<?php
class Helper {
    //Returns path to function from source path
    public static function getFullPathToFile(string $fileLocation = "") {
        return dirname(dirname(__FILE__)) . $fileLocation;
    }
}
