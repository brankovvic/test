<?php

include_once dirname(dirname(__FILE__)) . '/autoload.php';

class CacheCurrencyRatesCommand
{
    /**
     * @var array
     */
    private $currencyRate = [];
    /**
     * @var ExternalApiInterface
     */
    private $externalApi;
    /**
     * @var CurrencyRateRepository
     */
    private $currencyRateRepository;
    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;
    /**
     * @var ConnectionInterface
     */
    private $connection;
    /**
     * @var CurrencyRateCacheWrapperInterface
     */
    private $currencyRateCacheWrapper;

    /**
     * CacheCurrencyRatesCommand constructor.
     * @param ExternalApiInterface $externalApi
     * @param CurrencyRateRepository $currencyRateRepository
     * @param CurrencyRepository $currencyRepository
     * @param ConnectionInterface $connection
     * @param CurrencyRateCacheWrapperInterface $currencyRateCacheWrapper
     */
    public function __construct
    (
        ExternalApiInterface $externalApi,
        CurrencyRateRepository $currencyRateRepository,
        CurrencyRepository $currencyRepository,
        ConnectionInterface $connection,
        CurrencyRateCacheWrapperInterface $currencyRateCacheWrapper
    )
    {

        $this->externalApi = $externalApi;
        $this->currencyRateRepository = $currencyRateRepository;
        $this->currencyRepository = $currencyRepository;
        $this->connection = $connection;
        $this->currencyRateCacheWrapper = $currencyRateCacheWrapper;
    }

    public function getRates(): bool
    {
        try {
            $response = $this->externalApi->getExchangeRates();
            $this->connection->beginTransaction();
            $baseCurrencyAbbreviation = $response->source;
            //for simplicity i'm gonna use the abbreviation as name also

            $sourceCurrency = $this->findOrInsertCurrency($baseCurrencyAbbreviation);

            if (is_null($sourceCurrency)) throw new Exception("Failed To Insert Currency");


            foreach ($response->quotes as $currency => $rate) {
                $cleanName = CurrencyRepository::abbreviationFromString($baseCurrencyAbbreviation, $currency);

                $targetCurrencyAbbreviation = $this->findOrInsertCurrency($cleanName);

                $currencyRate = $this->currencyRateRepository->insertCurrencyRate($sourceCurrency->id(), $targetCurrencyAbbreviation->id(), $rate);

                if (is_null($currencyRate)) {
                    throw new Exception("Failed To Insert Currency Rate");
                }

                array_push($this->currencyRate, $currencyRate);
            }

            $this->connection->commit();
            //cache values in redis
            foreach ($this->currencyRate as $currencyRateModel) {
                //die(var_dump($currencyRateModel));
                $this->currencyRateCacheWrapper->setCurrencyRateModelInCache($currencyRateModel);
            }

            return true;

        } catch (Exception $exception) {
            //TODO Depends on how application handles errors
            $this->connection->rollBack();
            return false;
        }

    }

    public function findOrInsertCurrency(string $baseCurrencyAbbreviation): ?CurrencyInterface
    {
        $sourceCurrency = $this->currencyRepository->findByNameOrAbbreviation($baseCurrencyAbbreviation);

        if (is_null($sourceCurrency)) {
            $sourceCurrency = $this->currencyRepository->insertCurrency($baseCurrencyAbbreviation, $baseCurrencyAbbreviation);
        }

        return $sourceCurrency;
    }
}

