<?php
include_once dirname(dirname(__FILE__)) . '/autoload.php';
interface TradeRepositoryInterface {
    public function findById(int $id): ?TradeInterface;
    public function insertTrade(TradeInterface $trade): ?TradeInterface;
}

class TradeRepository implements TradeRepositoryInterface {
    /**
     * @var ConnectionInterface
     */
    private $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function findById(int $id): ?TradeInterface
    {
        $sql = "SELECT * FROM " . Trade::$tableName . " WHERE id={$id} LIMIT 1";

        $response = $this->connection->query($sql);

        return is_null($response) ? null : new Trade($response);
    }

    public function insertTrade(TradeInterface $trade): ?TradeInterface {

        $sql = "INSERT INTO " . Trade::$tableName . " 
        (trade_currency, exchange_rate, surcharge_percent, surcharge_amount, amount_paid, date_created) 
        VALUES (".$trade->tradeCurrency().", ".$trade->exchangeRate().", ".$trade->surchargePercentage().", 
        ".$trade->surchargeAmount().", ".$trade->amountPaid().", '".$trade->tradeCreated()."')";

        $responseId = $this->connection->query($sql);

        if((boolean)$responseId === false) return null;

        $trade->setId($responseId);

        return $trade;

    }
}