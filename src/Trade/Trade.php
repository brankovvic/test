<?php

include_once dirname(dirname(__FILE__)) . '/autoload.php';
interface TradeInterface {
    public function setId(int $id): void;
    public function id(): ?int ;
    public function tradeCurrency(): ?int;
    public function exchangeRate(): ?int ;
    public function surchargePercentage(): ?float ;
    public function surchargeAmount(): ?float ;
    public function amountPaid(): ?float ;
    public function tradeCreated(): ?string ;
}
class Trade implements TradeInterface {
    private $trade;

    public static $tableName="trade";

    public function __construct(stdClass $trade)
    {
        $this->trade = $trade;
    }

    public function setId(int $id): void {
        $this->trade->id = $id;
    }

    public function id(): ?int
    {
        return $this->trade->id;
    }

    public function tradeCurrency(): ?int
    {
        return (is_null($this->trade->trade_currency)) ? null : (int)$this->trade->trade_currency;
    }

    public function exchangeRate(): ?int
    {
        return (is_null($this->trade->exchange_rate)) ? null : (int)$this->trade->exchange_rate;
    }

    public function surchargePercentage(): ?float
    {
        return $this->trade->surcharge_percent;
    }

    public function surchargeAmount(): ?float
    {
        return $this->trade->surcharge_amount;
    }

    public function amountPaid(): ?float
    {
        return $this->trade->amount_paid;
    }

    public function tradeCreated(): ?string
    {
        return $this->trade->date_created;
    }
}