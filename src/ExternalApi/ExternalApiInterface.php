<?php

interface ExternalApiInterface {
    public function getExchangeRates(): ?stdClass;
}