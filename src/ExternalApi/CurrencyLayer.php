<?php
include_once dirname(dirname(__FILE__)) . '/autoload.php';


class CurrencyLayer implements ExternalApiInterface {
    /**
     * @var Config
     */
    private $config;

    public function __construct(Config $config)
    {

        $this->config = $config;
    }
    //TODO Again depend on the application logic how it handles the errors
    public function getExchangeRates(): ?stdClass {
        try {
            //die(var_dump($this->config->get('currencies')));
            $response = file_get_contents('http://apilayer.net/api/'.
                $this->config->get('endpoint').'?access_key='.$this->config->get('access_key').'&currencies='.
                $this->config->get('currencies'));
            return json_decode($response);
        } catch (Exception $exception) {
            return null;
        }
    }
}