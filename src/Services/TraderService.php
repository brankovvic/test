<?php

interface TraderServiceInterface {
    public function buyCurrency(Currency $sourceCurrency, Currency $targetCurrency, int $amount): ?bool;
}
class TraderService implements TraderServiceInterface {
    /**
     * @var Config
     */
    private $config;
    /**
     * @var CurrencyApi
     */
    private $currencyApi;
    /**
     * @var TradeRepositoryInterface
     */
    private $tradeRepository;

    public function __construct(Config $config, CurrencyApiInterface $currencyApi, TradeRepositoryInterface $tradeRepository)
    {

        $this->config = $config;
        $this->currencyApi = $currencyApi;
        $this->tradeRepository = $tradeRepository;
    }

    public function buyCurrency(Currency $sourceCurrency, Currency $targetCurrency, int $amount): ?bool
    {
        if ($amount === 0 ) return false;

        $surchargeForCurrency = $this->config->get($targetCurrency->abbreviation());

        //TODO This should be extracted inside a factory i will leave it like this for the sake of time
        $currencyRate = new stdClass();
        $currencyRate->source_currency = $sourceCurrency->id();
        $currencyRate->target_currency = $targetCurrency->id();

        $exchangeRate = $this->currencyApi->exchangeRateForCurrency(new CurrencyRate($currencyRate));

        $convertedAmountBefoureSurcharge = $amount / $exchangeRate;

        $surchargeAmount = $convertedAmountBefoureSurcharge * $surchargeForCurrency;

        $stdClass = new stdClass();
        $stdClass->trade_currency = $sourceCurrency->id();
        $stdClass->exchange_rate = $exchangeRate;
        $stdClass->surcharge_percent = $surchargeForCurrency;
        $stdClass->surcharge_amount = $surchargeAmount;
        $stdClass->amount_paid = $amount + $surchargeAmount;
        $stdClass->date_created =  (new DateTime())->format('Y-m-d H:i:s');

        $trader = new Trade($stdClass);

        return is_null($this->tradeRepository->insertTrade($trader)) ? null : true;
    }
}