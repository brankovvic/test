<?php
interface ConnectionInterface {
    public function getConnection(): mysqli;
    public function query(string $sql);
    public function beginTransaction(): void;
    public function commit(): void ;
    public function escapeString($value);
    public function rollBack(): void;
    public function getError();
    }
class Connection implements ConnectionInterface {
    private $mysqli = null;
    private $config = null;

    public function __construct(Config $config)
    {
        $this->config = $config;

        $this->mysqli = new MySQLi(
            $config->get('MYSQL_HOST'),
            $config->get('MYSQL_USER'),
            $config->get('MYSQL_PASSWORD')
        );

        if($this->mysqli->connect_error) {
            throw new Exception($this->mysqli->connect_errno);
        }
    }

    public function getConnection(): mysqli
    {
        return $this->mysqli;
    }

    //TODO Error needs to be logged so it can be used as a reference
    public function query(string $sql) {
        $this->mysqli->select_db($this->config->get('MYSQL_DATABASE'));
        $result = $this->mysqli->query($sql);
        if($result) {
            if (is_object($result)) {
                return $result->fetch_object();
            } else {
                return $this->mysqli->insert_id;
            }
        }
        return null;
    }

    public function getError() {
        return $this->mysqli->errno;
    }

    public function beginTransaction(): void
    {
        $this->mysqli->select_db($this->config->get('MYSQL_DATABASE'));

        $this->mysqli->begin_transaction();
    }

    public function commit(): void
    {
        $this->mysqli->select_db($this->config->get('MYSQL_DATABASE'));

        $this->mysqli->commit();
    }

    public function rollBack(): void {
        $this->mysqli->select_db($this->config->get('MYSQL_DATABASE'));

        $this->mysqli->rollback();
    }

    public function escapeString($value) {
        return $this->mysqli->real_escape_string($value);
    }
}