<?php

class Config {
    private $config = [];

    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    /**
     * @param string $parameterName
     * @return string|null
     */
    public function get(string $parameterName = "") {
        return ($this->config[$parameterName] !== null) ? $this->config[$parameterName] : null;
    }
}