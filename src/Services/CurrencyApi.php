<?php

interface CurrencyApiInterface {
    public function exchangeRateForCurrency(CurrencyRate $currencyRate): ?float;
}

/**
 * Class CurrencyApi
 */
class CurrencyApi implements CurrencyApiInterface {
    /**
     * @var CurrencyRateRepository
     */
    private $currencyRateRepository;
    /**
     * @var CurrencyRateCacheWrapperInterface
     */
    private $cacheWrapper;

    /**
     * CurrencyApi constructor.
     * @param CurrencyRateCacheWrapperInterface $cacheWrapper
     * @param CurrencyRateRepository $currencyRateRepository
     */
    public function __construct(CurrencyRateCacheWrapperInterface $cacheWrapper, CurrencyRateRepository $currencyRateRepository)
    {
        $this->currencyRateRepository = $currencyRateRepository;
        $this->cacheWrapper = $cacheWrapper;
    }

    /**
     * @param CurrencyRate $currencyRate
     * @return float|null
     */
    public function exchangeRateForCurrency(CurrencyRate $currencyRate): ?float
    {

        $currencyRateFromCache = $this->cacheWrapper->getCurrencyRateModelFromCache($currencyRate);

        if ( ! is_null($currencyRateFromCache)) return $currencyRateFromCache->rate();

        $currency = $this->currencyRateRepository->findBySourceAndTargetCurrency($currencyRate->sourceCurrencyId(), $currencyRate->targetCurrencyId());

        if(is_null($currency)) return null;

        $this->cacheWrapper->setCurrencyRateModelInCache($currency);

        return $currency->rate();
    }

}