<?php

interface CurrencyInterface {
    public function setCurrency(stdClass $currency): void;
    public function getCurrency(): stdClass;
    public function id(): ?int;
    public function name(): ?string;
    public function abbreviation(): ?string;
}

class Currency implements CurrencyInterface {
    static $tableName = "currencies";

    private $currency = null;

    public function __construct(stdClass $currency)
    {
        $this->currency = $currency;
    }

    public function setCurrency(stdClass $currency): void
    {
        $this->currency = $currency;
    }

    public function getCurrency(): stdClass
    {
        return $this->currency;
    }

    public function id() : ?int {
        return $this->currency->id;
    }

    public function name(): ?string
    {
        return $this->currency->name;
    }

    public function abbreviation(): ?string
    {
        return $this->currency->abbreviation;
    }
}