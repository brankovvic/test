<?php
include_once dirname(dirname(__FILE__)) . '/autoload.php';
interface CurrencyRepositoryInterface {
    public function findById(int $id): ?CurrencyInterface;
    public function insertCurrency(string $name, string $abbreviation): ?CurrencyInterface;
    public function findByNameOrAbbreviation(string $name): ?CurrencyInterface;
    public static function abbreviationFromString(string $abbreviation, string $string): ?string;
    public static function createObjectFromParameters(int $id, string $name, string $abbreviation): stdClass;
    }

class CurrencyRepository implements CurrencyRepositoryInterface {
    /**
     * @var ConnectionInterface
     */
    private $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    public function findById(int $id): ?CurrencyInterface
    {
        $sql = "SELECT * FROM " . Currency::$tableName . " WHERE id={$id} LIMIT 1";

        $response = $this->connection->query($sql);

        return is_null($response) ? null : new Currency($response);
    }

    public function insertCurrency(string $name, string $abbreviation): ?CurrencyInterface {
        $name = $this->connection->escapeString($name);

        $abbreviation = $this->connection->escapeString($abbreviation);

        $sql = "INSERT INTO " . Currency::$tableName . " (name, abbreviation) VALUES ('$name', '$abbreviation')";

        $responseId = $this->connection->query($sql);

        return ((boolean)$responseId === false) ? null : new Currency(self::createObjectFromParameters($responseId, $name, $abbreviation));

    }

    public function findByNameOrAbbreviation(string $name): ?CurrencyInterface {
        $name = $this->connection->escapeString($name);

        $sql = "SELECT * FROM " . Currency::$tableName . " WHERE name='{$name}' or abbreviation='{$name}' LIMIT 1";

        $response = $this->connection->query($sql);

        return is_null($response) ? null : new Currency($response);
    }

    /**
     * TODO This also should validate is $cleanAbbreviation is currency
     * @param string $sourceAbbreviation
     * @param string $string
     * @return string|null
     */
    public static function abbreviationFromString(string $sourceAbbreviation, string $string): ?string  {
        $cleanAbbreviation = trim(str_replace($sourceAbbreviation, '', $string));

        return (strlen($cleanAbbreviation) < 2) ? null : $cleanAbbreviation;
    }

    public static function createObjectFromParameters(int $id, string $name, string $abbreviation): stdClass {
        $stdClass = new stdClass();
        $stdClass->id = $id;
        $stdClass->name = $name;
        $stdClass->abbreviation = $abbreviation;
        return $stdClass;
    }
}