<?php
include_once dirname(dirname(__FILE__)) . '/Services/Config.php';
include_once dirname(dirname(__FILE__)) . '/Services/Connection.php';
include_once dirname(dirname(__FILE__)) . '/util/helper.php';
use PHPUnit\Framework\TestCase;

class ConnectionTest extends TestCase
{
    public function testItShouldConnectToDatabase()
    {
        $config = require dirname(dirname(__FILE__)) . '/config/database.php';

        $mock = $this->getMockBuilder(Config::class)->getMock();
        $mock->method('get')
            ->will($this->onConsecutiveCalls($config['MYSQL_HOST'], $config['MYSQL_USER'], $config['MYSQL_PASSWORD'], $config['MYSQL_DATABASE']));
        $connection = new Connection($mock);
        $this->assertInstanceOf(MySQLi::class, $connection->getConnection());
    }
}