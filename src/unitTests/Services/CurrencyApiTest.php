<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';

use PHPUnit\Framework\TestCase;

final class CurrencyApiTest extends TestCase {
    private $model;

    public function setUp(): void {
        $this->model = include dirname(dirname(__FILE__)) . '/../Example/Models/currencyRate.php';
    }

    public function tearDown(): void {
        $this->model = null;
    }

    public function testItShouldReturnValidCurrencyFromCache() {
        $cacheMock = $this->getMockBuilder(CurrencyRateCacheWrapper::class)
            ->disableOriginalConstructor()
            ->getMock();

        $cacheMock->method('getCurrencyRateModelFromCache')
            ->willReturn(new CurrencyRate($this->model));

        $currencyRateMock = $this->getMockBuilder(CurrencyRateRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $currencyApi = new CurrencyApi($cacheMock, $currencyRateMock);

        $currencyRate = $currencyApi->exchangeRateForCurrency(new CurrencyRate($this->model), new CurrencyRate($this->model));

        $this->assertEquals($currencyRate, $this->model->rate);

    }

    public function testItShouldReturnDataFromDatabaseIfValueIsNotFoundInCache() {
        $cacheMock = $this->getMockBuilder(CurrencyRateCacheWrapper::class)
            ->disableOriginalConstructor()
            ->getMock();

        $cacheMock->method('getCurrencyRateModelFromCache')
            ->willReturn(null);

        $currencyRateMock = $this->getMockBuilder(CurrencyRateRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $currencyRateMock->method('findBySourceAndTargetCurrency')->willReturn(new CurrencyRate($this->model));

        $currencyApi = new CurrencyApi($cacheMock, $currencyRateMock);

        $currencyRate = $currencyApi->exchangeRateForCurrency(new CurrencyRate($this->model), new CurrencyRate($this->model));

        $this->assertEquals($currencyRate, $this->model->rate);

    }

    public function testItShouldReturnNullIfValueIsNotFoundInCacheOrDatabase() {
        $cacheMock = $this->getMockBuilder(CurrencyRateCacheWrapper::class)
            ->disableOriginalConstructor()
            ->getMock();

        $cacheMock->method('getCurrencyRateModelFromCache')
            ->willReturn(null);

        $currencyRateMock = $this->getMockBuilder(CurrencyRateRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $currencyRateMock->method('findBySourceAndTargetCurrency')->willReturn(null);

        $currencyApi = new CurrencyApi($cacheMock, $currencyRateMock);

        $currencyRate = $currencyApi->exchangeRateForCurrency(new CurrencyRate($this->model), new CurrencyRate($this->model));

        $this->assertNull($currencyRate);
    }

}