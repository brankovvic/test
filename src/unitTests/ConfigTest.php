<?php
declare(strict_types=1);

include_once dirname(dirname(__FILE__)) . '/Services/Config.php';
include_once dirname(dirname(__FILE__)) . '/util/helper.php';

use PHPUnit\Framework\TestCase;

final class ConfigTest extends TestCase {
    public function testItShouldReturnNullForNonExistingConfigParameter() {
        $config = new Config();
        $this->assertNull($config->get('test'));
    }

    public function testItShouldReturnValidValueFromConfig()
    {
        $config = new Config(['test'=>'test']);
        $this->assertEquals('test', $config->get('test'));
    }

    public function testItShouldAlwaysReturnString() {
        $config = new Config(['test'=>1]);
        $this->assertEquals('1', $config->get('test'));
    }

    public function testItShouldFindDevelopmentConfig() {
        $config = include dirname(dirname(__FILE__)) .  '/config/database.php';
        $this->assertArrayHasKey('MYSQL_HOST' ,$config);
        $this->assertArrayHasKey('MYSQL_DATABASE' ,$config);
        $this->assertArrayHasKey('MYSQL_USER' ,$config);
        $this->assertArrayHasKey('MYSQL_PASSWORD' ,$config);
    }
}