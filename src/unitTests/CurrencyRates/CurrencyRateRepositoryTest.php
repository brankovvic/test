<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';
include_once dirname(dirname(__FILE__)) . '/../CurrencyRates/CurrencyRate.php';
use PHPUnit\Framework\TestCase;

final class CurrencyRateRepositoryTest extends TestCase {
    private $model;

    public function setUp(): void {
        $this->model = include dirname(dirname(__FILE__)) . '/../Example/Models/currencyRate.php';
    }

    public function tearDown(): void {
        $this->model = null;
    }
    public function testFindByIdItShouldReturnCurrencyObjectWithValidParameters()
    {

        $mock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('query')
            ->willReturn($this->model);

        $currencyRateRepository = new CurrencyRateRepository($mock);

        $currencyModel = new CurrencyRate($this->model);

        $this->assertEquals($currencyRateRepository->findById(1), $currencyModel);
    }

    public function testFindByIdItShouldReturnNullForNotFoundId()
    {
        $mock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('query')
            ->willReturn(null);

        $currencyRateRepository = new CurrencyRateRepository($mock);

        $this->assertNull($currencyRateRepository->findById(1));
    }


    public function testFindBySourceAndTargetCurrencyItShouldReturnValidObject() {
        $mock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('query')
            ->willReturn($this->model);

        $currencyRateRepository = new CurrencyRateRepository($mock);

        $currencyModel = new CurrencyRate($this->model);

        $this->assertEquals($currencyRateRepository->findBySourceAndTargetCurrency(1,1 ), $currencyModel);
    }

    public function testInsertCurrencyRateItShouldReturnValidCurrencyRate() {
        $mock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('query')
            ->willReturn($this->model->id);

        $currencyRateRepository = new CurrencyRateRepository($mock);

        $currencyModel = new CurrencyRate($this->model);
        $this->assertInstanceOf(CurrencyRate::class, $currencyRateRepository->insertCurrencyRate(1,1, 0.1));
    }

    public function testInsertCurrencyRateItShouldReturnNull() {
        $mock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('query')
            ->willReturn(null);

        $currencyRateRepository = new CurrencyRateRepository($mock);

        $this->assertNull($currencyRateRepository->insertCurrencyRate(1,1, 0.1));
    }
}