<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';
include_once dirname(dirname(__FILE__)) . '/../CurrencyRates/CurrencyRate.php';
use PHPUnit\Framework\TestCase;

final class CurrencyRateTest extends TestCase {
    public function testItShouldReturnAllValidParametersForCorrectData() {
        $currencyRateModelData = include dirname(dirname(__FILE__)) . '/../Example/Models/currencyRate.php';
        $currencyRateModel = new CurrencyRate($currencyRateModelData);
        $this->assertEquals($currencyRateModel->id(), (int)$currencyRateModelData->id);
        $this->assertEquals($currencyRateModel->sourceCurrencyId(), (int)$currencyRateModelData->source_currency);
        $this->assertEquals($currencyRateModel->targetCurrencyId(), (int)$currencyRateModelData->target_currency);
        $this->assertEquals($currencyRateModel->rate(), (float)$currencyRateModelData->rate);
    }

    public function testItShouldReturnNullForEmptyObject() {
        $currencyRateModel = new CurrencyRate(new stdClass());
        $this->assertNull($currencyRateModel->id());
        $this->assertNull($currencyRateModel->sourceCurrencyId());
        $this->assertNull($currencyRateModel->targetCurrencyId());
        $this->assertNull($currencyRateModel->rate());
    }
}