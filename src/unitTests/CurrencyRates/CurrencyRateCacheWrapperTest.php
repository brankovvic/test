<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';
include_once dirname(dirname(__FILE__)) . '/../CurrencyRates/CurrencyRate.php';

use PHPUnit\Framework\TestCase;

final class CurrencyRateCacheWrapperTest extends TestCase
{
    private $model;

    public function setUp(): void
    {
        $this->model = include dirname(dirname(__FILE__)) . '/../Example/Models/currencyRate.php';
    }

    public function tearDown(): void
    {
        $this->model = null;
    }

    public function testItShouldGetCurrencyRateModelFromCache() {
        $cacheMock = $this->getMockBuilder(Cache::class)
            ->disableOriginalConstructor()
            ->getMock();

        $cacheMock->method('existInCache')
            ->willReturn(true);
        $cacheMock->method('getFromCache')
            ->willReturn(json_encode((new CurrencyRate($this->model))));

        $currencyCacheWrapper = new CurrencyRateCacheWrapper($cacheMock);

        $currencyModel = new CurrencyRate($this->model);

        $this->assertEquals($currencyCacheWrapper->getCurrencyRateModelFromCache($currencyModel), $currencyModel);
    }

    public function testItShouldReturnNullForValueNotFoundInCache() {
        $cacheMock = $this->getMockBuilder(Cache::class)
            ->disableOriginalConstructor()
            ->getMock();

        $cacheMock->method('existInCache')
            ->willReturn(false);

        $currencyCacheWrapper = new CurrencyRateCacheWrapper($cacheMock);

        $this->assertNull($currencyCacheWrapper->getCurrencyRateModelFromCache(new CurrencyRate($this->model)));
    }
}