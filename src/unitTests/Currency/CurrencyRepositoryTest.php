<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';

use PHPUnit\Framework\TestCase;

final class CurrencyRepositoryTest extends TestCase
{
    private $model;

    public function setUp(): void {
        $this->model = include dirname(dirname(__FILE__)) . '/../Example/Models/currency.php';
    }
    public function testItShouldReturnObjectFromDatabaseConnection()
    {
        $mock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('query')
            ->willReturn($this->model);

        $repository = new CurrencyRepository($mock);

        $this->assertEquals($repository->findById(1), new Currency($this->model));
    }

    public function testItShouldReturnNullForNonExistingObject() {
        $mock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mock->method('query')
            ->willReturn(null);

        $repository = new CurrencyRepository($mock);

        $this->assertNull($repository->findById(1));
    }

    public function testItShouldReturnCurrencyObjectAfterInsert() {
        $mock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mock->method('escapeString')
            ->willReturn($this->model->abbreviation);

        $mock->method('query')
            ->willReturn($this->model->id);

        $currencyRepository = new CurrencyRepository($mock);

        $this->assertInstanceOf(Currency::class, $currencyRepository->insertCurrency($this->model->name, $this->model->name));

    }

    public function testItShouldReturnNullAfterInsertForInvalidData() {
        $mock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mock->method('escapeString')
            ->willReturn($this->model->abbreviation);

        $mock->method('query')
            ->willReturn(null);

        $currencyRepository = new CurrencyRepository($mock);

        $this->assertNull($currencyRepository->insertCurrency($this->model->name, $this->model->name));
    }

    public function testFindByNameItShouldReturnObjectIfValueExistInDatabase() {
        $mock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mock->method('escapeString')
            ->willReturn($this->model->abbreviation);

        $mock->method('query')
            ->willReturn($this->model);

        $currencyRepository = new CurrencyRepository($mock);

        $this->assertEquals($currencyRepository->findByNameOrAbbreviation($this->model->name), new Currency($this->model));
    }

    public function testFindByNameItShouldReturnNullIfValueDoesNotExistInDatabase() {
        $mock = $this->getMockBuilder(Connection::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mock->method('escapeString')
            ->willReturn($this->model->abbreviation);

        $mock->method('query')
            ->willReturn($this->model);

        $currencyRepository = new CurrencyRepository($mock);

        $this->assertEquals($currencyRepository->findByNameOrAbbreviation($this->model->name), new Currency($this->model));
    }

    public function testAbbreviationFromStringItShouldReturnValidAbbreviation() {
        $this->assertEquals(CurrencyRepository::abbreviationFromString('USD', 'USDEURO'), 'EURO');

    }

    public function testAbbreviationFromStringItShouldReturnNullForInvalidStringAbbreviation() {
        $this->assertNull(CurrencyRepository::abbreviationFromString('USD', 'USDO'));

    }
}
