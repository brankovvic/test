<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';

use PHPUnit\Framework\TestCase;

final class CurrencyTest extends TestCase
{
    public function testItShouldReturnValidDataForExistingModel()
    {
        $currencyModel = include dirname(dirname(__FILE__)) . '/../Example/Models/currency.php';

        $currency = new Currency($currencyModel);

        $this->assertEquals($currencyModel, $currency->getCurrency());

    }

    public function testItShouldReturnValidDataFromGetters() {
        $currencyModel = include dirname(dirname(__FILE__)) . '/../Example/Models/currency.php';

        $currency = new Currency($currencyModel);

        $this->assertEquals($currencyModel->name, $currency->name());
        $this->assertEquals($currencyModel->abbreviation, $currency->abbreviation());
        $this->assertEquals($currencyModel->id, $currency->id());
    }

    public function testItShouldReturnNullForNonExistingValue() {
        $currency = new Currency(new stdClass());

        $this->assertNull($currency->id());
        $this->assertNull($currency->name());
        $this->assertNull($currency->abbreviation());


    }
}