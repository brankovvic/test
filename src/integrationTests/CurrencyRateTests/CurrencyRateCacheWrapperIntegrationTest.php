<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';
include_once dirname(dirname(__FILE__)) . '/../CurrencyRates/CurrencyRate.php';

use PHPUnit\Framework\TestCase;

class CurrencyRateCacheWrapperIntegrationTest extends TestCase
{
    public function testItShouldSetCurrencyRateModelInCache() {
        $cache = new Cache();
        $currencyRateModel = new CurrencyRate(include dirname(dirname(__FILE__)) . '/../Example/Models/currencyRate.php');

        $currency = new CurrencyRateCacheWrapper($cache);
        $currency->setCurrencyRateModelInCache($currencyRateModel);

        $exists = $cache->getConnection()->exists(md5($currencyRateModel->sourceCurrencyId() . $currencyRateModel->targetCurrencyId()));

        $this->assertTrue((bool)$exists);
    }

    public function testItShouldReturnNullForNonExistingModel() {
        $cache = new Cache();
        $currencyRateModel = new CurrencyRate(include dirname(dirname(__FILE__)) . '/../Example/Models/currencyRate.php');

        $currency = new CurrencyRateCacheWrapper($cache);

        $this->assertInstanceOf(CurrencyRate::class, $currency->getCurrencyRateModelFromCache($currencyRateModel));
    }

    public function testItGetCurrencyRateModelFromCache() {
        $cache = new Cache();
        $currencyRateModel = new CurrencyRate(include dirname(dirname(__FILE__)) . '/../Example/Models/currencyRate.php');

        $currency = new CurrencyRateCacheWrapper($cache);
        $currency->setCurrencyRateModelInCache($currencyRateModel);

        $this->assertInstanceOf(CurrencyRate::class, $currency->getCurrencyRateModelFromCache($currencyRateModel));
    }

}