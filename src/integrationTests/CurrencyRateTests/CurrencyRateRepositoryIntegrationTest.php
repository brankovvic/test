<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';
include_once dirname(dirname(__FILE__)) . '/../CurrencyRates/CurrencyRate.php';

use PHPUnit\Framework\TestCase;

class CurrencyRateRepositoryIntegrationTest extends TestCase
{
    private $connection;

    private $model;

    private $migrate;

    public function setUp(): void {
        $this->connection = new Connection(new Config(include dirname(dirname(__FILE__)) . '/../config/database.php'));
        $this->model = include dirname(dirname(__FILE__)) . '/../Example/Models/currencyRate.php';
        $this->migrate = new Migrate($this->connection);
    }


    public function testInsertCurrencyRateShouldReturnValidObjectData() {
        $this->migrate->migrate();

        $currencyRepository = new CurrencyRateRepository($this->connection);

        $this->assertInstanceOf(CurrencyRate::class, $currencyRepository->insertCurrencyRate($this->model->source_currency, $this->model->target_currency, $this->model->rate));
    }


    public function testFindByIdShouldReturnNullForNonExistingData()
    {
        $this->migrate->migrate();

        $currencyRepository = new CurrencyRateRepository($this->connection);

        $this->assertNull($currencyRepository->findById(1));
    }

    public function testFindByIdShouldReturnCurrencyObjectForExistingData() {
        $this->migrate->migrate();

        $currencyRepository = new CurrencyRateRepository($this->connection);

        $inserted = $currencyRepository->insertCurrencyRate($this->model->source_currency, $this->model->target_currency, $this->model->rate);

        $this->assertInstanceOf(CurrencyRate::class, $currencyRepository->findById($inserted->id()));
    }
}