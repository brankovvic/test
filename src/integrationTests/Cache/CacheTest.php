<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';
include_once dirname(dirname(__FILE__)) . '/../CurrencyRates/CurrencyRate.php';

use PHPUnit\Framework\TestCase;

class CacheTest extends TestCase
{
    public function testSetInCacheShouldSetValue() {
        $cache = new Cache();

        $cache->getConnection()->flushAll();


        $this->assertTrue($cache->setInCache('test', 'test'));
        $connection = $cache->getConnection();

        $this->assertTrue((bool)$connection->exists('test'));
    }

    public function testGetFromCache() {
        $cache = new Cache();

        $cache->getConnection()->flushAll();


        $this->assertTrue($cache->setInCache('test', 'test'));

        $this->assertEquals($cache->getFromCache('test'), 'test');
    }

    public function testGetFromCacheShouldReturnFalseForInvalidData() {
        $cache = new Cache();

        $cache->getConnection()->flushAll();

        $this->assertFalse($cache->getFromCache('test'));
    }

    public function testExistInCacheShouldReturnTrue() {
        $cache = new Cache();

        $cache->getConnection()->flushAll();


        $this->assertTrue($cache->setInCache('test', 'test'));

        $this->assertTrue($cache->existInCache('test'));
    }

    public function testExistInCacheShouldReturnFalse() {
        $cache = new Cache();

        $cache->getConnection()->flushAll();

        $this->assertFalse($cache->existInCache('test'));
    }
}