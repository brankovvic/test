<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';
include_once dirname(dirname(__FILE__)) . '/../CurrencyRates/CurrencyRate.php';

use PHPUnit\Framework\TestCase;

class CurrencyRepositoryIntegrationTest extends TestCase
{
    private $connection;

    private $model;

    private $migrate;

    public function setUp(): void {
        $this->connection = new Connection(new Config(include dirname(dirname(__FILE__)) . '/../config/database.php'));
        $this->model = include dirname(dirname(__FILE__)) . '/../Example/Models/currency.php';
        $this->migrate = new Migrate($this->connection);
    }


    public function testInsertCurrencyShouldReturnNullForInvalidData() {
        $this->migrate->migrate();

        $currencyRepository = new CurrencyRepository($this->connection);

        $this->assertNull($currencyRepository->insertCurrency('USD', str_repeat('USD', 10000000)));
    }

    public function testInsertCurrencyShouldReturnCurrencyForValidData() {
        $this->migrate->migrate();

        $currencyRepository = new CurrencyRepository($this->connection);

        $currencyData = $currencyRepository->insertCurrency($this->model->abbreviation, $this->model->abbreviation);

        $this->assertInstanceOf(Currency::class, $currencyData);

        $this->assertEquals($currencyData->name(), $this->model->abbreviation);
        $this->assertEquals($currencyData->abbreviation(), $this->model->abbreviation);
    }

    public function testFindByIdShouldReturnNullForNonExistingData()
    {
        $this->migrate->migrate();

        $currencyRepository = new CurrencyRepository($this->connection);

        $this->assertNull($currencyRepository->findById(1));
    }

    public function testFindByIdShouldReturnCurrencyObjectForExistingData() {
        $this->migrate->migrate();

        $currencyRepository = new CurrencyRepository($this->connection);

        $inserted = $currencyRepository->insertCurrency($this->model->abbreviation, $this->model->abbreviation);

        $this->assertInstanceOf(Currency::class, $currencyRepository->findById($inserted->id()));
    }

    public function testFindByNameOrAbbreviationItShouldReturnNullForNonExistingData() {
        $this->migrate->migrate();

        $currencyRepository = new CurrencyRepository($this->connection);

        $this->assertNull($currencyRepository->findByNameOrAbbreviation($this->model->abbreviation));
    }

    public function testFindByNameOrAbbreviationItShouldReturnValidCurrencyObject()
    {
        $this->migrate->migrate();

        $currencyRepository = new CurrencyRepository($this->connection);

        $inserted = $currencyRepository->insertCurrency($this->model->abbreviation, $this->model->abbreviation);

        $currencyModel = $currencyRepository->findByNameOrAbbreviation($this->model->abbreviation);

        $this->assertInstanceOf(Currency::class, $currencyModel);

    }

    public function testCreateObjectFromParametersItShouldReturnValidObject() {
        $this->migrate->migrate();

        $currencyObject = CurrencyRepository::createObjectFromParameters($this->model->id, $this->model->name, $this->model->abbreviation);

        $this->assertEquals($this->model, $currencyObject);
    }
}