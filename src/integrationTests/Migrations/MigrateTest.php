<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';
include_once dirname(dirname(__FILE__)) . '/../CurrencyRates/CurrencyRate.php';

use PHPUnit\Framework\TestCase;

class MigrateTest extends TestCase
{
    public function testMigrateShouldMigrateData() {
        $configData = include dirname(dirname(__FILE__)) .  '/../config/database.php';

        $config = new Config($configData);
        $connection = new Connection($config);
        $migrate = new Migrate($connection);

        $migrate->migrate();

        $sql = "SELECT COUNT(*) as count FROM ".CurrencyRate::$tableName." LIMIT 1";

        $this->assertEquals("0", $connection->query($sql)->count);

        $migrate->rollback();
    }
}