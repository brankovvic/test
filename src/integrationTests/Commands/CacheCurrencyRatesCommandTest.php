<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';
include_once dirname(dirname(__FILE__)) . '/../CurrencyRates/CurrencyRate.php';

use PHPUnit\Framework\TestCase;

class CacheCurrencyRatesCommandTest extends TestCase
{

    //TODO Object creation with this chain should be wrapped in some factory
    public function testGetRatesShouldSaveInDatabaseAndInRedis() {
        $config = new Config(include dirname(dirname(__FILE__)) . '/../config/currencylayer.php');
        $database = new Connection(new Config(include dirname(dirname(__FILE__)) . '/../config/database.php'));
        $externalApi = new CurrencyLayer($config);
        $migrate = new Migrate($database);


        $currencyRateRepository = new CurrencyRateRepository($database);
        $currencyRepository = new CurrencyRepository($database);
        $cache = new Cache();
        $currencyRateCacheWrapper = new CurrencyRateCacheWrapper($cache);
        $cache->getConnection()->flushAll();
        $migrate->migrate();

        $command = new CacheCurrencyRatesCommand($externalApi, $currencyRateRepository, $currencyRepository, $database, $currencyRateCacheWrapper);

        $this->assertTrue($command->getRates());


        $this->assertNotNull($currencyRepository->findByNameOrAbbreviation('USD'));
        $this->assertNotNull($currencyRepository->findByNameOrAbbreviation('EUR'));
        $this->assertNotNull($currencyRepository->findByNameOrAbbreviation('GBP'));

        $this->assertNotNull($currencyRateRepository->findById(1));
        $this->assertNotNull($currencyRateRepository->findById(2));
        $this->assertNotNull($currencyRateRepository->findById(3));

        $stdClass = new stdClass();

        $stdClass->source_currency = 1;
        $stdClass->target_currency = 2;

        $this->assertNotNull($currencyRateCacheWrapper->getCurrencyRateModelFromCache(new CurrencyRate($stdClass)));




    }
}