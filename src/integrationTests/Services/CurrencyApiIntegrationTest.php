<?php


declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';
include_once dirname(dirname(__FILE__)) . '/../CurrencyRates/CurrencyRate.php';

use PHPUnit\Framework\TestCase;

class CurrencyApiIntegrationTest extends TestCase {
    public function testItShouldGetExchangeRateForCurrencyFromRedisCache() {
        $config = new Config(include dirname(dirname(__FILE__)) . '/../config/currencylayer.php');
        $database = new Connection(new Config(include dirname(dirname(__FILE__)) . '/../config/database.php'));

        $cache = new Cache();
        $cache->getConnection()->flushAll();

        $currencyRateCacheWrapper = new CurrencyRateCacheWrapper($cache);
        $currencyRateRepository = new CurrencyRateRepository($database);
        $currencyApi = new CurrencyApi($currencyRateCacheWrapper, $currencyRateRepository);

        $currencyRate = new CurrencyRate(include dirname(dirname(__FILE__)) . '/../Example/Models/currencyRate.php');

        $currencyRateCacheWrapper->setCurrencyRateModelInCache($currencyRate);

        $this->assertNotNull($currencyApi->exchangeRateForCurrency($currencyRate));
    }

    public function testItShouldGetExchangeRateFromDatabase() {
        $config = new Config(include dirname(dirname(__FILE__)) . '/../config/currencylayer.php');
        $database = new Connection(new Config(include dirname(dirname(__FILE__)) . '/../config/database.php'));
        $migrate = new Migrate($database);
        $cache = new Cache();

        $cache->getConnection()->flushAll();
        $migrate->migrate();

        $currencyRateCacheWrapper = new CurrencyRateCacheWrapper($cache);
        $currencyRateRepository = new CurrencyRateRepository($database);
        $currencyApi = new CurrencyApi($currencyRateCacheWrapper, $currencyRateRepository);

        $currencyRate = new CurrencyRate(include dirname(dirname(__FILE__)) . '/../Example/Models/currencyRate.php');

        $currencyRateRepository->insertCurrencyRate($currencyRate->sourceCurrencyId(), $currencyRate->targetCurrencyId(), $currencyRate->rate());

        $this->assertNotNull($currencyApi->exchangeRateForCurrency($currencyRate));
    }
}