<?php


declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';
include_once dirname(dirname(__FILE__)) . '/../CurrencyRates/CurrencyRate.php';

use PHPUnit\Framework\TestCase;

class TradeServiceIntegrationTest extends TestCase
{
    public function testItShouldBuyCurrency() {
        $surcharge = include dirname(dirname(__FILE__)) . '/../config/surcharge.php';

        $config = new Config($surcharge);
        $cache = new Cache();

        $cacheWrapper = new CurrencyRateCacheWrapper($cache);
        $currencyRate = new CurrencyRate(include dirname(dirname(__FILE__)) . '/../Example/Models/currencyRate.php');
        $database = new Connection(new Config(include dirname(dirname(__FILE__)) . '/../config/database.php'));

        $migrate = new Migrate($database);
        //$migrate->migrate();

        $currencyRateRepository = new CurrencyRateRepository($database);

        $currencyRateRepository->insertCurrencyRate($currencyRate->sourceCurrencyId(), $currencyRate->targetCurrencyId(), $currencyRate->rate());

        $tradeRepository = new TradeRepository($database);
        $currencyRateRepository = new CurrencyRateRepository($database);
        $currencyApi = new CurrencyApi($cacheWrapper, $currencyRateRepository);
        $tradeService = new TraderService($config, $currencyApi, $tradeRepository);

        $sourceCurrency = new Currency(include dirname(dirname(__FILE__)) . '/../Example/Models/currency.php');

        $targetCurrency = include dirname(dirname(__FILE__)) . '/../Example/Models/currency.php';
        $targetCurrency->id = 2;
        $targetCurrency->abbreviation = "EUR";
        $targetCurrency = new Currency($targetCurrency);
        $this->assertTrue($tradeService->buyCurrency($sourceCurrency, $targetCurrency, 100));
    }
}