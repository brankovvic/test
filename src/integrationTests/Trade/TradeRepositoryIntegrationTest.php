<?php


declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';

use PHPUnit\Framework\TestCase;

class TradeRepositoryIntegrationTest extends TestCase
{
    public function testItShouldEnterValidDataInsideADatabase()
    {
        $model = include dirname(dirname(__FILE__)) . '/../Example/Models/trade.php';
        $trade = new Trade($model);

        $database = new Connection(new Config(include dirname(dirname(__FILE__)) . '/../config/database.php'));

        //$migrate = (new Migrate($database))->migrate();

        $tradeRepository = new TradeRepository($database);

        $newTrade = $tradeRepository->insertTrade($trade);

        $this->assertEquals($newTrade, $trade);
    }

}