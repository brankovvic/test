<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';

use PHPUnit\Framework\TestCase;

class TradeIntegrationTest extends TestCase {
    public function testItShouldReturnAllValuesInDataObject() {
        $model = include dirname(dirname(__FILE__)) . '/../Example/Models/trade.php';
        $trade = new Trade($model);

        $this->assertEquals($trade->id(), $model->id);
        $this->assertEquals($trade->amountPaid(), $model->amount_paid);
        $this->assertEquals($trade->exchangeRate(), $model->exchange_rate);
        $this->assertEquals($trade->surchargeAmount(), $model->surcharge_amount);
        $this->assertEquals($trade->surchargePercentage(), $model->surcharge_percent);
        $this->assertEquals($trade->amountPaid(), $model->amount_paid);
    }
}
