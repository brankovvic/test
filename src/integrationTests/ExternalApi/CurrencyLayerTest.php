<?php
declare(strict_types=1);
include_once dirname(dirname(__FILE__)) . '/../autoload.php';
include_once dirname(dirname(__FILE__)) . '/../CurrencyRates/CurrencyRate.php';

use PHPUnit\Framework\TestCase;

class CurrencyLayerTest extends TestCase
{
    public function testItShouldReturnValidRates() {
        $configData = include dirname(dirname(__FILE__)) . '/../config/currencylayer.php';
        $config = new Config($configData);

        $currencyApi = new CurrencyLayer($config);

        $response = $currencyApi->getExchangeRates();

        $this->assertObjectHasAttribute('source', $response);
        $this->assertObjectHasAttribute('quotes', $response);
        $this->assertObjectHasAttribute('USDGBP', $response->quotes);
        $this->assertObjectHasAttribute('USDEUR', $response->quotes);
        $this->assertObjectHasAttribute('USDJPY', $response->quotes);
    }
}