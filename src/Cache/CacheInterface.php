<?php

interface CacheInterface {
    public function setInCache(string $key, $value): bool;
    public function getFromCache(string $key);
    public function existInCache(string $key): bool;
    public function getConnection(): ?Redis;
}