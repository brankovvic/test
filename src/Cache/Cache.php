<?php
include_once dirname(dirname(__FILE__)) . '/autoload.php';
class Cache implements CacheInterface {
    private $redis = null;

    public function __construct()
    {
        $this->redis = new Redis();
        $this->redis->connect('redis');
    }

    public function setInCache(string $key, $value): bool
    {
        return (bool)$this->redis->set($key, $value);
    }

    public function getFromCache(string $key)
    {
        return $this->redis->get($key);
    }

    public function existInCache(string $key): bool
    {
        return (bool)$this->redis->exists($key);
    }

    public function getConnection(): ?Redis
    {
        return $this->redis;
    }
}