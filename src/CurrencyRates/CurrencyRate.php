<?php

interface CurrencyRateInterface
{
    public function setCurrencyRateObject(stdClass $currencyRateObject): void;

    public function setSourceCurrencyId(int $currencyId): void;

    public function setTargetCurrencyId(int $currencyId): void;

    public function setRate(float $rate): void;

    public function sourceCurrencyId(): ?int;

    public function targetCurrencyId(): ?int;

    public function rate(): ?float;

    public function id(): ?int;
}

class CurrencyRate implements CurrencyRateInterface, JsonSerializable
{
    static $tableName = "currency_rates";
    /**
     * @var stdClass
     */
    private $currencyRate;

    /**
     * CurrencyRate constructor.
     * @param stdClass $currencyRate
     */
    public function __construct(stdClass $currencyRate)
    {

        $this->currencyRate = $currencyRate;
    }

    /**
     * @param stdClass $currencyRateObject
     */
    public function setCurrencyRateObject(stdClass $currencyRateObject): void {
        $this->currencyRate = $currencyRateObject;
    }

    /**
     * @param int $currencyId
     */
    public function setSourceCurrencyId(int $currencyId): void
    {
        $this->currencyRate['source_currency'] = $currencyId;
    }

    /**
     * @param int $currencyId
     */
    public function setTargetCurrencyId(int $currencyId): void
    {
        $this->currencyRate['target_currency'] = $currencyId;
    }

    /**
     * @param float $rate
     */
    public function setRate(float $rate): void
    {
        $this->currencyRate['rate'] = $rate;
    }

    /**
     * @return int
     */
    public function sourceCurrencyId(): ?int
    {
        return (is_null($this->currencyRate->source_currency)) ? null : (int)$this->currencyRate->source_currency;
    }

    /**
     * @return int
     */
    public function targetCurrencyId(): ?int
    {
        return (is_null($this->currencyRate->target_currency)) ? null : (int)$this->currencyRate->target_currency;
    }

    /**
     * @return float
     */
    public function rate(): ?float
    {
        return (is_null($this->currencyRate->rate)) ? null : (float)$this->currencyRate->rate;
    }

    /**
     * @return int
     */
    public function id(): ?int
    {
        return (is_null($this->currencyRate->id)) ? null : (int)$this->currencyRate->id;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}