<?php


include_once dirname(dirname(__FILE__)) . '/autoload.php';

interface CurrencyRateRepositoryInterface
{
    public function findById(int $id): ?CurrencyRateInterface;

    public function findBySourceAndTargetCurrency(int $sourceCurrency, int $targetCurrency): ?CurrencyRateInterface;

    public function insertCurrencyRate(int $sourceCurrency, int $targetCurrency, float $rate): ?CurrencyRateInterface;
}

//TODO Repository can be extracted to abstract class to reuse methods
class CurrencyRateRepository implements CurrencyRateRepositoryInterface
{
    /**
     * @var ConnectionInterface
     */
    private $connection;

    public function __construct(ConnectionInterface $connection)
    {

        $this->connection = $connection;
    }

    public function findById(int $id): ?CurrencyRateInterface
    {
        $sql = "SELECT * FROM " . CurrencyRate::$tableName . " where id={$id} LIMIT 1";

        $data = $this->connection->query($sql);

        return is_null($data) ? null : new CurrencyRate($data);
    }

    public function findBySourceAndTargetCurrency(int $sourceCurrency, int $targetCurrency): ?CurrencyRateInterface
    {
        $sql = "SELECT * FROM " . CurrencyRate::$tableName .
            " WHERE source_currency={$sourceCurrency} 
                AND target_currency={$targetCurrency} LIMIT 1";

        $data = $this->connection->query($sql);

        return is_null($data) ? null : new CurrencyRate($data);
    }

    public function insertCurrencyRate(int $sourceCurrency, int $targetCurrency, float $rate): ?CurrencyRateInterface {

        $sql = "INSERT INTO " . CurrencyRate::$tableName . " (source_currency, target_currency, rate) " .
            "VALUES ($sourceCurrency, $targetCurrency, $rate)";

        $responseId = $this->connection->query($sql);

        return ((boolean)$responseId === false) ? null : new CurrencyRate(self::createObjectFromParameters($responseId, $sourceCurrency, $targetCurrency, $rate));
    }

    public static function createObjectFromParameters(int $id, int $sourceId, int $targetId, float $rate): stdClass {
        $stdClass = new stdClass();
        $stdClass->id = $id;
        $stdClass->source_currency = $sourceId;
        $stdClass->target_currency = $targetId;
        $stdClass->rate = $rate;
        return $stdClass;
    }
}