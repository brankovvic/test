<?php

interface CurrencyRateCacheWrapperInterface {
    public function getCurrencyRateModelFromCache(CurrencyRate $currencyRate): ?CurrencyRateInterface;

    public function setCurrencyRateModelInCache(CurrencyRateInterface $currencyRate): void;
}

class CurrencyRateCacheWrapper implements CurrencyRateCacheWrapperInterface {
    /**
     * @var CacheInterface
     */
    private $cache;

    public function __construct(CacheInterface $cache)
    {

        $this->cache = $cache;
    }

    public function getCurrencyRateModelFromCache(CurrencyRate $currencyRate): ?CurrencyRateInterface
    {
        if( ! $this->cache->existInCache(md5($currencyRate->sourceCurrencyId() . $currencyRate->targetCurrencyId()))) return null;

        $currencyRate = $this->cache->getFromCache(md5($currencyRate->sourceCurrencyId() . $currencyRate->targetCurrencyId()));

        $currencyRate = json_decode($currencyRate);

        return new CurrencyRate($currencyRate->currencyRate);

    }

    public function setCurrencyRateModelInCache(CurrencyRateInterface $currencyRate): void
    {
        $this->cache->setInCache(md5($currencyRate->sourceCurrencyId() . $currencyRate->targetCurrencyId()), json_encode($currencyRate));

    }
}