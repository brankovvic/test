<?php

$currency = new stdClass();

$currency->id = 1;
$currency->trade_currency=1;
$currency->exchange_rate=1;
$currency->surcharge_percent = 0.75;
$currency->surcharge_amount = 2.3;
$currency->surcharge_amount = 2.3;
$currency->amount_paid = 100;
$currency->date_created = (new DateTime())->format('Y-m-d H:i:s');

return $currency;