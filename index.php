<?php
class Configuration
{
    static $COMPANYAPIURL = 'https://5f27781bf5d27e001612e057.mockapi.io/webprovise/companies';
    static $TRAVELAPIURL = 'https://5f27781bf5d27e001612e057.mockapi.io/webprovise/travels';

}
class Request
{
    public function makeRequest(string $apiEndpoint): ?array
    {
        try {
            $content = file_get_contents($apiEndpoint);
            return json_decode($content, true);
        } catch (Exception $exception) {
            return null;
        }
    }
}

class Travel
{
    private $travelData;

    public function __construct($travelData)
    {
        $this->travelData = $travelData;
    }

    public function calculateTotalCost($companyId)
    {
        $totalCost = 0;

        foreach ($this->travelData as $trip) {
            if ($trip['companyId'] === $companyId) {
                $totalCost += (float) $trip['price'];
            }
        }

        return $totalCost;
    }
}

class Company
{
    private $companiesData;
    private $travel;

    public function __construct($companiesData, Travel $travel)
    {
        $this->companiesData = $companiesData;
        $this->travel = $travel;
    }

    public function buildNestedArray($companyId)
    {
        $companyInfo = [];

        foreach ($this->companiesData as $company) {
            if ($company['id'] === $companyId) {
                $companyInfo = $company;
                break;
            }
        }

        $companyInfo['cost'] = $this->travel->calculateTotalCost($companyId);
        $companyInfo['children'] = [];

        foreach ($this->companiesData as $childCompany) {
            if ($childCompany['parentId'] === $companyId) {
                $childInfo = $this->buildNestedArray($childCompany['id']);
                $companyInfo['children'][] = $childInfo;
            }
        }

        return $companyInfo;
    }
}

class TestScript
{
    public function execute()
    {
        $start = microtime(true);

        $request = new Request();

        $travelData = $request->makeRequest(Configuration::$TRAVELAPIURL);

        if ($travelData === null) {
            throw new Exception("Bad api request");
        }

        $travel = new Travel($travelData);

        $companiesData = $request->makeRequest(Configuration::$COMPANYAPIURL);

        if ($companiesData === null) {
            throw new Exception("Bad api request");
        }

        $company = new Company($companiesData, $travel);

        $result = [];
        foreach ($companiesData as $rootCompany) {
            if ($rootCompany['parentId'] === "0") {
                $result[] = $company->buildNestedArray($rootCompany['id']);
            }
        }

        echo json_encode($result, JSON_PRETTY_PRINT);
        echo 'Total time: '.  (microtime(true) - $start);
    }
}

(new TestScript())->execute();

