Test Application For Fetching User GeoLocation
==============

This is a simple application that allow you to buy currency.
Currency currencies are limited and only works for USD  EUR JPY...

Notes:

Given that i was low on time so i couldn't complete the whole application...
Application is missing the frontend part but the first iteration for backend is done.
Application have unit and integration tests, if some of the tests are missing or some cases are not
missing apologizes for that...
Also on the second run i will go over the app and create better folder structure etc...
But i think that you can find from this sample test how i think and work.


Setup Application
----------------------------

Application is inside a docker so you should get a docker first:

.. code-block:: console

    https://www.docker.com/

After installing docker you should run command, command will build and run docker

.. code-block:: console

    docker-compose up --build

Running Tests
-----------------------------

If docker is up and all the dependecies are installed tests needs to be run to insure
that application is working as intended

Run phpunit unit tests:

.. code-block:: console

    docker exec -it example_php-fpm_1 bash
    $cd ..
    $phpunit unitTests/

Run phpunit integration tests:

.. code-block:: console

    docker exec -it example_php-fpm_1 bash
    $cd ..
    $phpunit integrationTests/

If all tests are successful you're now ready to use application.


Running Application
-----------------------------
The application doesnt have user interface yet


Enjoy!